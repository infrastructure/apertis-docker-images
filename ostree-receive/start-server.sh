#!/bin/sh

set -ex

rm -rf /run/sshd
mkdir -p /run/sshd

getent group archive >/dev/null || groupadd archive -g ${ARCHIVE_GID:-1000}
getent passwd archive >/dev/null || \
  useradd archive -mp '*' -u ${ARCHIVE_UID:-1000} -g ${ARCHIVE_GID:-1000} \
    -s /opt/ostree-receive/shell.sh
exec /usr/sbin/sshd -Dep ${PORT:-2222} "$@"
