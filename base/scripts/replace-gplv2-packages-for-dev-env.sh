#!/bin/sh
set -e

cp /bin/rm /bin/tar /bin/grep /usr/bin/diff /usr/local/bin
apt-get update
DPKGPATH=/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
apt-get -y -o Dpkg::Path="$DPKGPATH" \
  -o APT::Get::Allow-Solver-Remove-Essential=1 \
  -o APT::Get::Allow-Remove-Essential=1 \
  install coreutils diffutils findutils grep gzip sed tar
rm /usr/local/bin/rm /usr/local/bin/tar /usr/local/bin/grep /usr/local/bin/diff